""" Programa principal para la solucion del ejercicio
         17.4 "Aplicación Redirectora" """

import socket
import random

# Función donde prepara la respuesta HTTP de redirección
def obtener_url(lista_urls):
	url_aleatoria = random.choice(lista_urls)
	respuesta = "HTTP/1.1 302 Found\r\n" \
				+ "Location:" + url_aleatoria + "\r\n\r\n"
	return respuesta

# Funcion principal donde se ejecuta el servidor
def main():
	# Lista de urls a las que se puede redirigir
	lista_urls = ['https://www.aulavirtual.urjc.es/', 'https://www.urjc.es/',
				  'https://www.elmundo.es/', 'https://stackoverflow.com/']

	# Creación y configuración del socket
	mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	mySocket.bind(('localhost', 5678))

	# Numero de conexiones TCP entrantes
	mySocket.listen(5)

	try:
		while True:
			print("Esperando conexion...")
			(recvSocket, address) = mySocket.accept()
			print("Solicitud HTTP recibida:")
			print(recvSocket.recv(2048))
			recvSocket.send(obtener_url(lista_urls).encode('ascii'))
			recvSocket.close()
	except KeyboardInterrupt:
		print("\nCerrando socket")
		mySocket.close()

if __name__ == "__main__":
	main()